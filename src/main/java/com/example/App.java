package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.starwars.API;
import com.example.starwars.GetRequestRepository;
import com.example.starwars.Printer;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(App.class, args);
    	
        API apiCalls = new API();
        GetRequestRepository repository = new GetRequestRepository(apiCalls);
        Printer printer = new Printer();

        // -------------------------------------------
        // Ejemplo: Imprimir los detalles de la pelicula "The Phantom Menace"
        //
       // JsonObject jsonObject = repository.getAll("films", "Phantom");
        JsonObject jsonObject = repository.getAll("films", null);
        
  
    
    
        JsonArray filmresults = jsonObject.getAsJsonArray("results");
       // printer.printDetailsFilms(filmresults);

        // -------------------------------------------
        // Ejercicio 1: Imprimir en consola el Nombre del personaje, y el nombre de su especie, el cual haya aparecido en la mayor cantidad de peliculas.
        // Ejemplo:
        //
        //   Anakin Skywalker - Human
        //
     
       
      
     
        printer.printEjercicio1(filmresults);

        // -------------------------------------------
        // Ejercicio 2: Imprimir en consola una tabla resumen de todas las naves que ha piloteado "Obi-Wan Kenobi" en cada pelicula
        // Ejemplo:
        //
        //  Titulo Pelicula       | Nombre nave/vehiculo      | Tipo
        //  ----------------------|---------------------------|-------------------------
        //  Attack of the Clones  | Jedi starfighter          | starships
        //  Revenge of the Sith   | Trade Federation cruiser  | starships
        //  Revenge of the Sith   | Trade Federation cruiser  | starships
        //  The Phantom Menace    | Tribubble bongo           | vehicles
   
        
        
        JsonObject jsonObjectLeiaOrgana = repository.getAll("people", null);
        JsonArray jsonArray = jsonObjectLeiaOrgana.getAsJsonArray("results");
        JsonObject obiWanKenobi = jsonArray.get(9).getAsJsonObject();
        //
        printer.printEjercicio2(obiWanKenobi);
        
        
   

        // -------------------------------------------
        // Ejercicio 3 (OPCIONAL): Modifique esta aplicacion para convertirla en una aplicacion web, la cual imprima una pagina HTML los resultados de los ejercicios anteriores.
        // Se recomienda utilizar String Framework (https://spring.io/guides/gs/spring-boot/)
        //


    }
}
