package com.example.spring.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.spring.model.VehiculoDTO;
import com.example.starwars.API;
import com.example.starwars.GetRequestRepository;
import com.example.starwars.Printer;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Controller
public class printController {

	private List<VehiculoDTO> listaVehiculos;	
	 Printer printer = new Printer();
	 API apiCalls = new API();
     GetRequestRepository repository = new GetRequestRepository(apiCalls);
	@RequestMapping(value = {"/", "/print"})
    public String imprimirEjercicios(Model model){
	
       
        
        JsonObject jsonObject = repository.getAll("films", null);
        JsonArray filmresults = jsonObject.getAsJsonArray("results");
		JsonObject reponse = printer.printEjercicio1(filmresults);
		String nombreEspecie = "";
		
		if(reponse.get("species").getAsJsonArray() != null) {
			JsonObject especie = repository.innerRequest(reponse.get("species").getAsString());
			nombreEspecie = especie.get("name").getAsString();
		}

		model.addAttribute("nombre", reponse.get("name"));
        model.addAttribute("especie", nombreEspecie);
        /////////////////////////////////////////////////////////////////////////////////////////////////////
    	JsonObject jsonObjectLeiaOrgana = repository.getAll("people", null);
        JsonArray jsonArray = jsonObjectLeiaOrgana.getAsJsonArray("results");
        JsonObject obiWanKenobi = jsonArray.get(9).getAsJsonObject();
        model.addAttribute("name",obiWanKenobi.get("name"));
        
        
        return "print";
    }

	@ModelAttribute("vehiculos")
    public List<VehiculoDTO> getVehicules() {
		JsonObject jsonObjectLeiaOrgana = repository.getAll("people", null);
        JsonArray jsonArray = jsonObjectLeiaOrgana.getAsJsonArray("results");
        JsonObject obiWanKenobi = jsonArray.get(9).getAsJsonObject();
        listaVehiculos = printer.printEjercicio2(obiWanKenobi);
        return listaVehiculos;
    }



}
