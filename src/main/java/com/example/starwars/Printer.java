package com.example.starwars;

import java.util.ArrayList;
import java.util.List;

import com.example.spring.model.VehiculoDTO;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Printer {

    public Printer() {
    }

    API api = new API();
    GetRequestRepository repository = new GetRequestRepository(api);
    List<String> listaNombresPersonajes = new ArrayList<String>();
    List<String> listaNombres = new ArrayList<String>();
    List<JsonObject> listaCharacter = new ArrayList<JsonObject>();
    List<JsonObject> listaCharacterUniq = new ArrayList<JsonObject>();

    public void printDetailsFilms(JsonArray results) {

        if (results.size() != 0) {

            for (int i = 0; i < results.size(); i++) {

                JsonObject film = results.get(i).getAsJsonObject();
                System.out.println("Title : " + film.get("title"));
                System.out.println("episode number : " + film.get("episode_id"));
                JsonElement crawl = film.get("opening_crawl");
                String crawlfixed = crawl.toString().replace("\\r\\n", " ");
                System.out.println("Opening crawl : " + crawlfixed);
                System.out.println("Director : " + film.get("director"));
                System.out.println("Producer : " + film.get("producer"));
                System.out.println("Release Date : " + film.get("release_date"));

                JsonArray characters = film.getAsJsonArray("characters");
                System.out.println("");
                System.out.println("characters :");
                System.out.println("");
                printSubCall("name", characters);
                System.out.println("");

                JsonArray planets = film.getAsJsonArray("planets");
                System.out.println("");
                System.out.println("planets :");
                System.out.println("");
                printSubCall("name", planets);
                System.out.println("");

                JsonArray starships = film.getAsJsonArray("starships");
                System.out.println("");
                System.out.println("starships :");
                System.out.println("");
                printSubCall("name", starships);
                System.out.println("");

                JsonArray vehicles = film.getAsJsonArray("vehicles");
                System.out.println("");
                System.out.println("vehicles :");
                System.out.println("");
                printSubCall("name", vehicles);
                System.out.println("");

                JsonArray species = film.getAsJsonArray("species");
                System.out.println("");
                System.out.println("species :");
                System.out.println("");
                printSubCall("name", species);
                System.out.println("");

                System.out.println("");
                System.out.println("");
            }
        } else {
            System.out.println("Your search didn't get any results");
        }
    }

    public void printDetailsPlanets(JsonArray planetresults) {

        if (planetresults.size() != 0)
            for (int i = 0; i < planetresults.size(); i++) {
                JsonObject planet = planetresults.get(i).getAsJsonObject();
                System.out.println("Planet : " + planet.get("name"));
                System.out.println("Rotation Period : " + planet.get("rotation_period"));
                System.out.println("Orbital Period : " + planet.get("orbital_period"));
                System.out.println("Diameter : " + planet.get("diameter"));
                System.out.println("Gravity : " + planet.get("gravity"));
                System.out.println("Terrain : " + planet.get("terrain"));
                System.out.println("Surface water : " + planet.get("surface_water"));
                System.out.println("Population : " + planet.get("population"));

                JsonArray residents = planet.getAsJsonArray("residents");
                System.out.println("");
                System.out.println("Residents :");
                System.out.println("");
                printSubCall("name", residents);
                System.out.println("");

                JsonArray films = planet.getAsJsonArray("films");
                System.out.println("");
                System.out.println("Films :");
                System.out.println("");
                printSubCall("title", films);
                System.out.println("");
            }
        else {
            System.out.println("Your search didn't get any results");
        }
    }

    public void printDetailsStarships(JsonArray starshipresults) {
        if (starshipresults.size() != 0)
            for (int i = 0; i < starshipresults.size(); i++) {

                JsonObject starship = starshipresults.get(i).getAsJsonObject();

                System.out.println("Name : " + starship.get("name"));
                System.out.println("Model : " + starship.get("model"));
                System.out.println("Manufacturer : " + starship.get("manufacturer"));
                System.out.println("Cost in credits : " + starship.get("cost_in_credits"));
                System.out.println("Length : " + starship.get("length"));
                System.out.println("Max Atmosphering Speed : " + starship.get("max_atmosphering_speed"));
                System.out.println("Crew : " + starship.get("crew"));
                System.out.println("Passengers : " + starship.get("passengers"));
                System.out.println("Cargo Capacity : " + starship.get("cargo_capacity"));
                System.out.println("Consumables : " + starship.get("consumables"));
                System.out.println("Hyperdrive Rating : " + starship.get("hyperdrive_rating"));
                System.out.println("MGLT : " + starship.get("MGLT"));
                System.out.println("Starship Class : " + starship.get("starship_class"));

                JsonArray pilots = starship.getAsJsonArray("pilots");
                System.out.println("");
                System.out.println("Pilots :");
                System.out.println("");
                printSubCall("name", pilots);
                System.out.println("");

                JsonArray films = starship.getAsJsonArray("films");
                System.out.println("");
                System.out.println("Films :");
                System.out.println("");
                printSubCall("title", films);
                System.out.println("");
            }
        else {
            System.out.println("Your search didn't get any results");
        }
    }

    //prints the underlying api calls in the array of the original call.
    private void printSubCall(String entity, JsonArray jsonArray) {
        if (jsonArray.size() != 0) {
            for (int j = 0; j < jsonArray.size(); j++) {
                JsonElement character = jsonArray.get(j);
                String uri = character.getAsString();
                JsonObject response = repository.innerRequest(uri);
                boolean existe = false;
                listaCharacter.add(response);
                for (int i = 0; i < listaCharacterUniq.size(); i++) {
					if(response.get(entity).getAsString().equals(listaCharacterUniq.get(i).get(entity).getAsString())) {
						existe = true;
					}
				}
                
                if(!existe) {
                	listaCharacterUniq.add(response);
                }
                existe = false;
                
            	//System.out.println(response.get(entity));	
            	
            }
        } else {
            System.out.println("nothing here");
            
        }
    }




	public JsonObject printEjercicio1(JsonArray filmresults) {
		System.out.println("Solución ejercicio1 :");
		for (int i = 0; i < filmresults.size(); i++) {
			JsonObject film = filmresults.get(i).getAsJsonObject();
		    printSubCall("name", film.getAsJsonArray("characters"));
		   
			 	
		}
		
	   int count1 = 0;
   		int count2 = 0;
   		String nombrePersonaje = "";
   		String nombreEspecie = "";
   		JsonObject jsonObjectReturn = null;
   		
   		for (int j = 0; j < listaCharacterUniq.size(); j++) {
   			//System.out.println(listaCharacterUniq.get(j).get("name"));
   			
				for (int i = 0; i < listaCharacter.size(); i++) {
					  //System.out.println(listaCharacter.get(i).get("name"));
					
					if(listaCharacterUniq.get(j).get("name").equals(listaCharacter.get(i).get("name"))) {
						count1++;
					}
				}
			
			
			if(count1 >= count2) {
				nombrePersonaje = listaCharacterUniq.get(j).get("name").getAsString();
				if(listaCharacterUniq.get(j).get("species").getAsJsonArray() != null) {
					JsonObject especie = repository.innerRequest(listaCharacterUniq.get(j).get("species").getAsString());
					nombreEspecie = especie.get("name").getAsString();
				}
				jsonObjectReturn = listaCharacterUniq.get(j);
				
				count2 = count1;
			}
			count1 = 0;
			
		}
	   System.out.println("Nombre Personaje :"+nombrePersonaje);
	   System.out.println("Especie Personaje :"+nombreEspecie);
           
		
		

		
		return jsonObjectReturn;
		
	}

	public List<VehiculoDTO> printEjercicio2(JsonObject obiWanKenobi) {
		List<VehiculoDTO> listaVehiculos = new ArrayList<>();
		VehiculoDTO vehiculoDTO = null; 
		System.out.println("");
		System.out.println("");
		System.out.println("Solución ejercicio2 :");
		System.out.println("");
		System.out.println("");
		System.out.println("Nombre Personaje :"+obiWanKenobi.get("name"));
		System.out.println("");
		System.out.println("Titulo Pelicula       | Nombre nave/vehiculo      | Tipo");
		/**
		 * se obtienen las peliculas asociadas a Obi-Wan Kenobi
		 */
		JsonArray filmsArray = obiWanKenobi.getAsJsonArray("films");
		/**
		 * se obtienen las naves asociadas a Obi-Wan Kenobi
		 */
		JsonArray starshipsArray = obiWanKenobi.getAsJsonArray("starships");
		/**
		 * se obtienen las vehiculos asociadas a Obi-Wan Kenobi
		 */
		JsonArray vehiclesArray = obiWanKenobi.getAsJsonArray("vehicles");
	
		for (int i = 0; i < filmsArray.size(); i++) {
			
			JsonObject pelicula = repository.innerRequest(filmsArray.get(i).getAsString());
			
			 for (int j = 0; j < pelicula.getAsJsonArray("vehicles").size(); j++) { 
				
				 for (int x = 0; x < vehiclesArray.size(); x++) {
					if(vehiclesArray.get(x).getAsString().equals(pelicula.getAsJsonArray("vehicles").get(j).getAsString())) {
					
						JsonObject vehiculo = repository.innerRequest(vehiclesArray.get(x).getAsString());
						vehiculoDTO = new VehiculoDTO(vehiculo.get("name").getAsString()+"   vehicles", pelicula.get("title").getAsString());
						listaVehiculos.add(vehiculoDTO);
						System.out.println(pelicula.get("title")+"      "+vehiculo.get("name")+"     "+"vehicles");
					}
				}
		
			}
			 
			 
			 for (int j = 0; j < pelicula.getAsJsonArray("starships").size(); j++) { 
					
				 for (int a = 0; a < starshipsArray.size(); a++) {
						if(starshipsArray.get(a).getAsString().equals(pelicula.getAsJsonArray("starships").get(j).getAsString())) {
							
							JsonObject nave = repository.innerRequest(starshipsArray.get(a).getAsString());
							vehiculoDTO = new VehiculoDTO(nave.get("name").getAsString()+"    starships", pelicula.get("title").getAsString());
							listaVehiculos.add(vehiculoDTO);
							System.out.println(pelicula.get("title")+"     "+nave.get("name")+"     "+"starships");
						}
					}
			}
		
			
		}
	
		
		return listaVehiculos;
		
	}
	

}
