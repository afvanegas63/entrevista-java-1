package com.example.spring.model;

public class VehiculoDTO {
	
	private String nombre;
	private String tituloPelicula;
	
	
	
	public VehiculoDTO(String nombre, String tituloPelicula) {
		super();
		this.nombre = nombre;
		this.tituloPelicula = tituloPelicula;
	}
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTituloPelicula() {
		return tituloPelicula;
	}
	public void setTituloPelicula(String tituloPelicula) {
		this.tituloPelicula = tituloPelicula;
	}
	
	

}
